package com.nono.movies

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.nono.movies.databinding.ActivityLoginBinding

class LoginActivity : AppCompatActivity() {
    lateinit var binding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.loginButton.setOnClickListener{
            if (validLogin(binding.usernameEditText.text.toString(),
                    binding.passwordEditText.text.toString())) {
                        Toast.makeText(this@LoginActivity, "login mdp text ok", Toast.LENGTH_SHORT).show()
                        navigateToMovieList()

            } else {
                Toast.makeText(this@LoginActivity, "login mdp text pas ok", Toast.LENGTH_SHORT).show()
            }

        }
    }

    private fun validLogin(username: String, password: String): Boolean {
        return username == "kotlin" && password == "rocks"
    }

    private fun navigateToMovieList() {
        val intent = Intent(this, MovieListActivity::class.java)
        startActivity(intent)
        finish()

    }
}