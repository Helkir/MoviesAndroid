package com.nono.movies

enum class Genre {
    SF,
    COMEDY,
}

data class Movie (
    val title: String,
    val genre: String,
    val releaseDate: String,
    val posterId: Int
)