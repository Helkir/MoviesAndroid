package com.nono.movies.movielist

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.nono.movies.Movie
import com.nono.movies.databinding.ItemMovieBinding

class MovieAdapter(private var movies: List<Movie>) : RecyclerView.Adapter<MovieAdapter.ViewHolder>()  {
    class ViewHolder(val binding: ItemMovieBinding): RecyclerView.ViewHolder(binding.root) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemMovieBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val movie = movies[position]
        holder.binding.titleTextView.text = movie.title
        holder.binding.genreTextView.text = "TBD"
        holder.binding.releaseDateTextView.text = movie.releaseDate
        holder.binding.posterImageView.setImageResource(movie.posterId)
    }

    override fun getItemCount(): Int = movies.size
}